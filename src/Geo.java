public class Geo {
    private String city;
    private Long latitude;
    private Long longitude;
    private Integer accuracy_radius;

    public String getCity() {
        return city;
    }

    public Long getLatitude() {
        return latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public Integer getAccuracy_radius() {
        return accuracy_radius;
    }
}
