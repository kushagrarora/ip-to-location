public class Location {
    private FullIp fullip;

    public FullIp getFullip() {
        return fullip;
    }

    public String getCity() {
        return this.getFullip().getGeo().getCity();
    }

    public Long getLatitude() {
        return this.getFullip().getGeo().getLatitude();
    }

    public Long getLongitude() {
        return this.getFullip().getGeo().getLatitude();
    }

    public Integer getAccuracy_radius() {
        return this.getFullip().getGeo().getAccuracy_radius();
    }
}
