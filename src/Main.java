import com.google.gson.Gson;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

class Main{
    public static void main(String[] args) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request1 = HttpRequest.newBuilder()
                .uri(URI.create("http://checkip.amazonaws.com")).GET()
                .build();

        HttpResponse response1 = HttpClient.newBuilder()
                .build()
                .send( request1, HttpResponse.BodyHandlers.ofString() );

        String localIp = response1.body().toString().trim();

        System.out.println("Searching for your location...");

        HttpRequest request = HttpRequest.newBuilder()
                .header("X-Auth-Token", "04cc9e62-ef69-4060-a9f5-76ffe313cfcb")
                .uri(URI.create("https://signals.api.auth0.com/v2.0/ip/" + localIp)).GET()
                .build();

        HttpResponse response = HttpClient.newBuilder()
                .build()
                .send( request, HttpResponse.BodyHandlers.ofString() );

        Gson g = new Gson();
        Location location = g.fromJson(response.body().toString(), Location.class);

        System.out.println("City : " + location.getCity());
        System.out.println("Latitude : " + location.getLatitude());
        System.out.println("Longitude : " + location.getLongitude());
        System.out.println("Accuracy radius (in KM) : " + location.getAccuracy_radius());
    }
}
